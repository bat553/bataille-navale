#! /usr/bin/env python3
# coding: UTF-8
"""
Script: outil.py
"""
import string
import random
import os
import pickle

def indices_vers_case_ln(indices):
    alphabet = string.ascii_lowercase.upper()
    if indices[0] > 9 or indices[1] > 9:
        return False
    else:
        return str(alphabet[indices[0]]) + str(indices[1] + 1)

def case_ln_vers_indices(ln):
    alphabet = "ABCDEFGHIJ"
    l = alphabet.index(ln[0])
    return [l, int(ln[1:3])-1]

def est_case_valide(case):
    if test_est_case_erreur(case)==True:
        L = case[0]
        n = int(case[1:3])
        if not isinstance(L, str) or not isinstance(n, int):
            return False
        if n > 10:
            return False
        elif not L.isupper():
            return False
        elif not L in "ABCDEFGHIJ":
            return False
        else:
            return True
    return False

def case_aleatoire():
    L = string.ascii_uppercase
    return L[random.randint(0,9)] + str(random.randint(1,10))

def joueur_aleatoire():
    joueurs = ['moi','adversaire']
    return joueurs[random.randint(0,1)]

def affichage_statistiques_joueurs():
    if os.path.isfile("statistiques.txt")==False:
        return ""
    else:
        Taille=0
        stats = open("statistiques.txt", "r")
        for stat in stats.readlines():
            l = stat.split(";")
            a=len(l[0])
            if Taille<a:
                Taille=a
        stats.close()
        if Taille>=6:
            tableau = "+-----------+----------------+-----------+----------+----------+\n"
            tableau += "| Joueurs   | Parties jouées | Victoires | Défaites | Abandons |\n"
            tableau += "+===========+================+===========+==========+==========+\n"
            stats = open("statistiques.txt", "r")
            for stat in stats.readlines():
                l = stat.split(";")
                a = len(l[3]) - 1
                l.append(int(l[1]) - int(l[2]) - int(l[3]))
                tableau += "| {:<10}| {:<15}| {:<10}| {:<9}| {:<9}|\n".format(l[0], l[1], l[2], l[3][:a], l[4])
                tableau += "+-----------+----------------+-----------+----------+----------+\n"
        else:
            tableau = "+---------+----------------+-----------+----------+----------+\n"
            tableau += "| Joueurs | Parties jouées | Victoires | Défaites | Abandons |\n"
            tableau += "+=========+================+===========+==========+==========+\n"
            stats = open("statistiques.txt", "r")
            for stat in stats.readlines():
                l = stat.split(";")
                a = len(l[3]) - 1
                l.append(int(l[1]) - int(l[2]) - int(l[3]))
                tableau += "| {:<8}| {:<15}| {:<10}| {:<9}| {:<9}|\n".format(l[0], l[1], l[2], l[3][:a], l[4])
                tableau += "+---------+----------------+-----------+----------+----------+\n"
        stats.close()

        return tableau

def ajoute_statistiques_joueur(pseudo, resultat):
    if os.path.isfile("statistiques.txt") == False:
        stats=open("statistiques.txt","w")
        if resultat == "gagne":
            stat = pseudo + ";1;1;0\n"
        elif resultat == "perd":
            stat = pseudo + ";1;0;1\n"
        else:
            stat = pseudo + ";1;0;0\n"

    else:
        stat = ""
        stats = open("statistiques.txt", "r")
        z=0
        for stat2 in stats.readlines():
            l = stat2.split(";")
            if l[0]==pseudo:
                if resultat == "gagne":
                    a=int(l[1])+1
                    b=int(l[2])+1
                    stat += pseudo + ";" + str(a) + ";" + str(b)+ ";" + l[3]
                elif resultat == "perd":
                    a = int(l[1]) + 1
                    b = int(l[2]) + 1
                    stat += pseudo + ";" + str(a) + ";" + l[2] + ";" + str(b)+"\n"

                else:
                    a = int(l[1]) + 1
                    stat += pseudo + ";" + str(a) + ";" + l[2] + ";" + l[3]
                z=1
            else:
                if resultat == "gagne":
                    stat += stat2
                elif resultat == "perd":
                    stat += stat2
                else:
                    stat += stat2
        if z==0:
            if resultat == "gagne":
                stat += pseudo + ";1;1;0\n"
            elif resultat == "perd":
                stat += pseudo + ";1;0;1\n"
            else:
                stat += pseudo + ";1;0;0\n"

        stats.close()
        stats = open("statistiques.txt", "w")
    stats.write(stat)
    stats.close()

def test_est_case_erreur(self):
    nbr = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    for nbr2 in nbr:
        if (len(self)!=2 and len(self)!=3):
            return False
        if self[1] == nbr2:
            return True
    return False

# Fonctions à développer selon le cahier des charges

# Programme principal pour tester vos fonctions
def main():
    est_case_valide("stop")

if __name__ == '__main__':
    main()