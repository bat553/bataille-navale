#! /usr/bin/env python3
# coding: UTF-8
"""
Script: jeux_console.py
"""
import reseau
import flottes
import outils
import terminal

def bataille_manuel(ma_flotte, sa_flotte, joueur):
    joueur_qui_joue = joueur
    est_finie = False # Booléen indiquant si la partie est finie

    while not est_finie: # Boucle de jeu
        action = None
        # quelle flotte est-celle du joueur ?
        print("Joueur : ", joueur_qui_joue)
        if joueur_qui_joue == "moi":
            while not action:
                action = terminal.saisie_cible_valide_ou_commande(None)
                if action == "stop": return "abandonne"

            # quelles sont les conséquences de l'action demandée sur la flotte ?
            resultat = flottes.analyse_tir(sa_flotte, action)
            print(resultat)

            # la partie est-elle finie ?
            if resultat == "gagne":
                return "gagne"
            else:
                joueur_qui_joue = "adversaire"
        else:
            while not action:
                action = terminal.saisie_cible_valide_ou_commande(None)
                if action == "stop": return "abandonne"

            # quelles sont les conséquences de l'action demandée sur la flotte ?
            resultat = flottes.analyse_tir(ma_flotte, action)
            print(resultat)

        # la partie est-elle finie ?
            if resultat == "gagne":
                return "perd"
            else:
                joueur_qui_joue = "moi"

def bataille_auto(ma_flotte, sa_flotte, joueur, mode_jeu):
    joueur_qui_joue = joueur
    est_finie = False  # Booléen indiquant si la partie est finie

    while not est_finie:  # Boucle de jeu
        action = None
        # quelle flotte est-celle du joueur ?
        print("Joueur : ", joueur_qui_joue)
        if joueur_qui_joue == "moi":
            while not action:
                action = terminal.saisie_cible_valide_ou_commande(None)
                if action == "stop": return "abandonne"

            # quelles sont les conséquences de l'action demandée sur la flotte ?
            resultat = flottes.analyse_tir(sa_flotte, action)

            # la partie est-elle finie ?
            if resultat == "gagne":
                return "gagne"
            else:
                joueur_qui_joue = "adversaire"
        else:
            action = flottes.tir_aleatoire(ma_flotte)
            # quelles sont les conséquences de l'action demandée sur la flotte ?
            resultat = flottes.analyse_tir(ma_flotte, action)

            if resultat == "gagne":
                return "perd"
            else:
                joueur_qui_joue = "moi"


def initialisation_bataille_reseau(ma_flotte, sa_flotte, ip, port):
    socket = reseau.connexion_serveur(ip, port)
    if socket:
        gg=""
        while gg!="[fin]":
            gg=reseau.recuperation_commande_serveur(socket)
            if gg=="[pseudo]":
                pseudo = input("Saisir votre pseudo :")
                ma_flotte["pseudo"] = pseudo
                commande = '[pseudo]' + ma_flotte["pseudo"]
                reseau.repond_au_serveur(socket, commande)
            elif gg == "[attente]":
                commande = '[ack]'
                reseau.repond_au_serveur(socket, commande)
            elif "[start]" in gg:
                commande_parse = reseau.parseur_commande(gg)
                sa_flotte["pseudo"] = commande_parse[1]
                commande = '[ack]'
                reseau.repond_au_serveur(socket, commande)
                return socket
            elif gg == "[timeout]":
                return None
    return None

def bataille_reseau_tir_recu(ma_flotte, sa_flotte, case, socket_actif, no_iteration):
    test = flottes.analyse_tir(ma_flotte, case)
    liste = "[resultat]" + case + "|" + test
    reseau.repond_au_serveur(socket_actif, liste)
    print(no_iteration)
    return test


def bataille_reseau_envoyer_tir(ma_flotte, sa_flotte, socket_actif, no_iteration):
    print(no_iteration)
    case=input("Entrer une case")
    if case =="stop":
        reseau.repond_au_serveur(socket_actif,"[fin]")
        return case
    reseau.repond_au_serveur(socket_actif,"[tir]"+case)
    commande = reseau.recuperation_commande_serveur(socket_actif)
    commande = reseau.parseur_commande(commande)
    a = commande[2]
    reseau.repond_au_serveur(socket_actif,"[ack]")
    sa_flotte["tirs"].append(commande[1])
    sa_flotte["effets"].append(a)
    if a in ["touche","coule","gagne"]:
        sa_flotte["nbreTouche"]+=1
        if a in ["coule","gagne"]:
            sa_flotte["nbreCoule"] += 1
    return a

def bataille_reseau(ma_flotte, ip, port):
    sa_flotte=flottes.initialisation_flotte_vide()
    socket=initialisation_bataille_reseau(ma_flotte,sa_flotte,ip,port)
    nbr=0
    if socket:
        while True:
            nbr+=1
            commande=reseau.recuperation_commande_serveur(socket)
            if commande == '[cible]':
                res=bataille_reseau_envoyer_tir(ma_flotte,sa_flotte,socket,nbr)
                if res == "stop":
                    return "abandonne"
                elif res =="gagne":
                    return "gagne"
            elif "[tir]" in commande:
                case=reseau.parseur_commande(commande)
                case=case[1]
                resultat=bataille_reseau_tir_recu(ma_flotte,sa_flotte,case,socket,nbr)
                if resultat == "gagne":
                    return "perd"
            elif commande =="[fin]":
                return "abandonne"

def choix_flotte():
    mode = None
    while not mode:
        mode = terminal.saisie_mode_initialisation_flottes()
        if mode == "endur":
            return [flottes.initialisation_flotte_par_dictionnaire_fixe('flotte1'), flottes.initialisation_flotte_par_dictionnaire_fixe('flotte2')]
        elif mode == "manuel":
            ma_flotte = terminal.choix_flotte_manuel_console(flottes.initialisation_flotte_vide())
            sa_flotte = flottes.initialisation_flotte_vide()
            return [ma_flotte, sa_flotte]
        elif mode == "manuel+aleatoire":
            ma_flotte = terminal.choix_flotte_manuel_console(flottes.initialisation_flotte_vide())
            sa_flotte = flottes.choix_flotte_aleatoire(flottes.initialisation_flotte_vide())
            return [ma_flotte, sa_flotte]
        elif mode == "aleatoires":
            ma_flotte = flottes.choix_flotte_aleatoire(flottes.initialisation_flotte_vide())
            sa_flotte = flottes.choix_flotte_aleatoire(flottes.initialisation_flotte_vide())
            return [ma_flotte, sa_flotte]
        elif mode == "restaurees":
            ma_flotte=flottes.initialisation_flotte_vide()
            sa_flotte=flottes.initialisation_flotte_vide()
            backup = flottes.restauration_partie(ma_flotte, sa_flotte)
            if backup:
                return [ma_flotte, sa_flotte]
            else:
                mode = None

def partie():
    OK = False
    c_flotte = choix_flotte()
    ma_flotte = c_flotte[0]
    sa_flotte = c_flotte[1]
    while not OK:
        c_mode = terminal.saisie_mode_choix_jeu()
        if c_mode == 'ia':
            print('IA Non disponible')
        else:
            OK = True
    if c_mode == "reseau":
        params_res = terminal.saisie_adresse_reseau()
        resultat=bataille_reseau(ma_flotte, params_res[0], params_res[1])
        pseudo = terminal.saisie_pseudo()
    elif c_mode == "manuel" or c_mode == "auto":
        pseudo = terminal.saisie_pseudo()
        joueur = outils.joueur_aleatoire()
        if c_mode=="manuel":
            resultat=bataille_manuel(ma_flotte, sa_flotte, joueur)
        elif c_mode == "auto":
            resultat=bataille_auto(ma_flotte, sa_flotte, joueur, c_mode)
    outils.ajoute_statistiques_joueur(pseudo, resultat)
    outils.affichage_statistiques_joueurs()



# Fonctions à développer selon le cahier des charges

# Programme principal pour tester vos fonctions
def main():
    print(partie())



if __name__ == '__main__':
    main()
