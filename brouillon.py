def bataille_reseau_tir_recu(ma_flotte, sa_flotte, case, socket_actif, no_iteration):
    test = flottes.analyse_tir(ma_flotte, case)
    liste = "[resultat]" + case + "|" + test
    reseau.repond_au_serveur(socket_actif, liste)
    return test

def bataille_reseau(ma_flotte, ip, port):
    return reseau.repond_commande_au_serveur(initialisation_bataille_reseau(ma_flotte,"",ip,port), "[resultat]")