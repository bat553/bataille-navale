#! /usr/bin/env python3
# coding: UTF-8
"""
Script: terminal.py
"""

import flottes
import outils
import couleurs


def affiche_flotte_textuelle(flotte):
    # Affiche sur la console le descriptif de la flotte
    # INIT

    for i in flotte['bateaux']:
        # Type de bateaux
        print("   >", i, ":", end=" ")
        for pos in flotte['bateaux'][i]:
            lenght = flotte['bateaux'][i].__len__()
            if flotte['bateaux'][i][lenght - 1] == pos:
                # Si c'est la 1ere postion, pas de virgule au début
                print(pos, end="\n")
            else:
                print(pos + ", ", end="")


def affiche_flotte_2d(flotte, cachee=False):
    grille = "   | 1 2 3 4 5 6 7 8 9 10|\n" + "---|---------------------|\n"
    bateaux = flottes.positions_bateaux(flotte)
    lignes = "ABCDEFGHIJ"

    for ligne in lignes:
        grille += " " + ligne + " |"
        for col in range(1, 11):
            if (ligne + str(col)) in flotte["tirs"] and cachee:
                # Position des tirs
                if ligne + str(col) not in bateaux:
                    lettre = "O"
                else:
                    lettre = "X"

                grille += " " + lettre
            elif (ligne + str(col)) in flotte["tirs"]:
                # Position des tirs
                if ligne + str(col) not in bateaux:
                    lettre = "O"
                elif bateaux[ligne + str(col)] == "contre-torpilleurs":
                    lettre = "Q"
                else:
                    lettre = bateaux[ligne + str(col)][0].upper()
                grille += " " + lettre

            elif (ligne + str(col)) in bateaux and not cachee:
                # Position des bateaux
                if bateaux[ligne + str(col)] == "contre-torpilleurs":
                    lettre = "q"
                else:
                    lettre = bateaux[ligne + str(col)][0]
                grille += " " + lettre

            else:
                grille += "  "
        grille += " |\n"

    grille += "---|---------------------|"
    print(grille)


# Fonctions à développer selon le cahier des charges
def saisie_cible_valide_ou_commande(commandes):
    while True:
        saisie = input("Saisr une case valide au format Ln ou une commande(stop ou sauv)")
        if saisie == "stop" or saisie == "sauv":
            return saisie
        elif outils.est_case_valide(saisie) == True:
            return saisie


def affiche_flotte_2d_couleurs(flotte, cachee=False):
    print("   | 1 2 3 4 5 6 7 8 9 10|\n" + "---|---------------------|\n",end="")
    bateaux = flottes.positions_bateaux(flotte)
    lignes = "ABCDEFGHIJ"
    colonnes = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    for ligne in lignes:
        print("{:^3}| ".format(ligne), end="")
        for colonne in colonnes:
            indiceln = ligne + colonne
            if indiceln in flotte["tirs"]:
                if indiceln  not in flottes.positions_bateaux(flotte).keys():
                    print("O ", end="")
                elif cachee == True:
                    print(couleurs.COULEURS["rouge"] + "X", end="")
                    print(couleurs.COULEURS["blanc"] + ' ', end="")
                else:
                    if bateaux[indiceln] == "porte-avions":
                        print(couleurs.COULEURS["bleu"] + "X" , end="")
                    elif bateaux[indiceln] == "croiseur":
                        print(couleurs.COULEURS["cyan"] + "X" , end="")
                    elif bateaux[indiceln] == "contre-torpilleurs":
                        print(couleurs.COULEURS["jaune"] + "X" , end="")
                    elif bateaux[indiceln] == "sous-marin":
                        print(couleurs.COULEURS['vert'] + "X", end="")
                    elif bateaux[indiceln] == "torpilleur":
                        print(couleurs.COULEURS['magenta'] + "X", end="")
                    print(couleurs.COULEURS["blanc"] + ' ', end="")
            elif indiceln in bateaux:
                if cachee == False:
                    if bateaux[indiceln] == "porte-avions":
                        print(couleurs.COULEURS["bleu"] + "#" , end="")
                    elif bateaux[indiceln] == "croiseur":
                        print(couleurs.COULEURS["cyan"] + "#" , end="")
                    elif bateaux[indiceln] == "contre-torpilleurs":
                        print(couleurs.COULEURS["jaune"] + "#" , end="")
                    elif bateaux[indiceln] == "sous-marin":
                        print(couleurs.COULEURS['vert'] + "#" , end="")
                    elif bateaux[indiceln] == "torpilleur":
                        print(couleurs.COULEURS['magenta'] + "#" , end="")
                    print(couleurs.COULEURS["blanc"] + ' ', end="")
                else:
                    print("{:>2}".format(""), end="")
            else:
                print("{:>2}".format(""), end="")
        print("|")
    print("---|---------------------|")

def saisie_direction_valide():
    saisie =True
    direction_valide=["h","v","horizontal","vertical"]
    while saisie == True:
        direction=input("Entrer une direction:")
        if direction in direction_valide :
            direction=direction.lower()
            if direction == "h":
                direction = direction_valide[2]
            elif direction == "v":
                direction =direction_valide[3]
            saisie == False
            return direction

def affiche_flotte_inconnue_2d(flotte):
    affiche_flotte_2d_couleurs(flotte,True)
    touche =flotte["nbreTouche"]
    print()
    print("Indicateurs : {:1} touches / {} bateau(x) coulé(s)".format(int(touche),flotte["nbreCoule"]))

def choix_bateau_a_positionner(flotte):
    saisie = True
    ct =flotte["bateaux"]["contre-torpilleurs"]
    c =flotte["bateaux"]["croiseur"]
    pa=flotte["bateaux"]["porte-avions"]
    sm=flotte["bateaux"]["sous-marin"]
    t=flotte["bateaux"]["torpilleur"]
    afficher="Choisir le bateau à (re)positionner parmi :\n"
    afficher+= "   1) contre-torpilleurs (3 cases), actuellement "
    if len(ct) == 0:afficher += "non positionné"+"\n"
    else:afficher += "en "+", ".join(ct)+"\n"
    afficher+= "   2) croiseur (4 cases), actuellement "
    if len(c) == 0:afficher += "non positionné"+"\n"
    else:afficher += "en "+", ".join(c)+"\n"
    afficher+= "   3) porte-avions (5 cases), actuellement "
    if len(pa) == 0:afficher += "non positionné"+"\n"
    else:afficher += "en "+", ".join(pa)+"\n"
    afficher+= "   4) sous-marin (3 cases), actuellement "
    if len(sm) == 0:afficher += "non positionné"+"\n"
    else:afficher += "en "+", ".join(sm)+"\n"
    afficher+= "   5) torpilleur (2 cases), actuellement "
    if len(t) == 0:afficher += "non positionné"+"\n"
    else:afficher += "en "+", ".join(t)+"\n"
    afficher+="Taper stop pour stopper le choix de la flotte"
    print(afficher)
    possible = ["1","2","3","4","5"]
    while saisie ==True:
        position=input()
        if position in possible:
            return int(position)
        if flottes.est_flotte_complete(flotte) ==True:
            return "stop"

def saisie_mode_initialisation_flottes():
    dico = {
        "1": "endur",
        "2": "manuel",
        "3": "manuel+aleatoire",
        "4": "aleatoires",
        "5": "restaurees"
    }
    ok = False
    while not ok:
        print('\nMode de choix de la flotte :\n----------------------------\n\
 1> flottes initialisées en dur\n\
 2> flotte du joueur initialisée manuellement et flotte de l\'adversaire vide\n\
 3> flotte du joueur initialisée manuellement et flotte de l\'adversaire aléatoirement\n\
 4> flottes initialisées aléatoirement (defaut)\n\
 5> flottes restaurées de la dernière partie sauvegardée\n\n', end="")

        choix = input('Votre choix ?')
        if dico.get(choix):
            ok = True
    return dico.get(choix)


def saisie_adresse_reseau():
    ok = False
    while not ok:
        saisies = input('Adresse du serveur [hostname:port]?')
        saisies = saisies.split(':')

        if saisies.__len__() == 2:
            try :
                port = int(saisies[1])
                hostname = saisies[0]
                if hostname and port:
                    return [hostname, port]
            except ValueError:
                pass
def choix_flotte_manuel_console(flotte):
    bateaux = {
        1: "contre-torpilleurs",
        2: "croiseur",
        3: "porte-avions",
        4: "sous-marin",
        5: "torpilleur"
    }
    while True:
        action = choix_bateau_a_positionner(flotte)
        if action == 'stop':
            return flotte
        print("Choisir la case de début du", bateaux.get(action))
        case = saisie_cible_valide_ou_commande(['stop'])
        direction = saisie_direction_valide()
        if flotte['bateaux'][bateaux.get(action)].__len__():
            print("-> positionnement du", bateaux.get(action), "échoué")
            print("La flotte en 2D :")
            affiche_flotte_2d_couleurs(flotte, False)
            print()
        else:
            flottes.positionne_bateau_par_direction(flotte, bateaux.get(action), case, direction)
            position_effective = flottes.calcul_positions_bateau(case, direction, flottes.longueur_bateau(bateaux.get(action)))
            print("-> positionnement du",  bateaux.get(action), "en", ", ".join(position_effective), "effectué")
            print("La flotte en 2D :")
            affiche_flotte_2d_couleurs(flotte, False)
            print()

def saisie_pseudo():
    pseudo=""
    while pseudo=="":
        pseudo=input("Entrez votre pseudo :")
    return pseudo

def saisie_mode_choix_jeu():
    nbr=0
    while True:
        print("\nMode de choix du jeu :\n----------------------\n 1> jeu entièrement manuel [utile pour le debug] (choix par defaut)\n 2> jeu automatique contre l'ordinateur\n 3> jeu automatique avancé contre l'ordinateur (tir avec IA)\n 4> jeu en réseau\n")
        saisie=input("")
        if saisie=="1" or nbr==4:return "manuel"
        elif saisie=="2":return "auto"
        elif saisie=="3":return "ia"
        elif saisie=="4":return "reseau"
        nbr+=1
# Programme principal pour tester vos fonctions
def main():
    choix_flotte_manuel_console(flottes.initialisation_flotte_vide())

if __name__ == '__main__':
    main()
