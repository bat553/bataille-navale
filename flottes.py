#! /usr/bin/env python3
# coding: UTF-8
"""
Script: flottes.py
"""
import os
import pickle
import outils
import random

# Fonctions à développer selon le cahier des charges
def initialisation_flotte_par_dictionnaire_fixe(nom):
    if nom == "flotte1":#Verifie si le nom correspond
        flotte = {'bateaux': {'porte-avions': ["A1", "B1", "C1", "D1", "E1"],
                              'croiseur': ["A3", "A4", "A5", "A6"],
                              'contre-torpilleurs': ["J8", "J9", "J10"],
                              'sous-marin': ["F1", "F2", "F3"],
                              'torpilleur': ["D2", "E2"]},
                  'tirs': [],
                  'effets': [],
                  'pseudo': 'moi',
                  'nbreTouche': 0,
                  'nbreCoule': 0}
    elif nom == "flotte2":
        flotte = {'bateaux': {'porte-avions': ["A1", "A2", "A3", "A4", "A5"],
                              'croiseur': ["B1", "B2", "B3", "B4"],
                              'contre-torpilleurs': ["D1", "D2", "D3"],
                              'sous-marin': ["E1", "E2", "E3"],
                              'torpilleur': ["H3", "H4"]},
                  'tirs': [],
                  'effets': [],
                  'pseudo': 'adversaire',
                  'nbreTouche': 0,
                  'nbreCoule': 0}
    elif nom == "flotte_bateau_touche":
        flotte = {'bateaux': {'porte-avions': ["A1", "B1", "C1", "D1", "E1"],
                              'croiseur': ["A3", "A4", "A5", "A6"],
                              'contre-torpilleurs': ["J8", "J9", "J10"],
                              'sous-marin': ["F1", "F2", "F3"],
                              'torpilleur': ["D2", "E2"]},
                  'tirs': ["A3", "A4", "A5"],
                  'effets': ["touche", "touche", "touche"],
                  'pseudo': 'moi',
                  'nbreTouche': 3,
                  'nbreCoule': 0}
    elif nom == "flotte_presque_coulee":
        flotte = {'bateaux': {'porte-avions': ["A1", "A2", "A3", "A4", "A5"],
                              'croiseur': ["B1", "B2", "B3", "B4"],
                              'contre-torpilleurs': ["D1", "D2", "D3"],
                              'sous-marin': ["E1", "E2", "E3"],
                              'torpilleur': ["H3", "H4"]},
                  'tirs': ["A1", "A2", "A3", "A4", "A5",
                           "B1", "B2", "B3", "B4",
                           "D1", "D2", "D3",
                           "E1", "E2", "E3",
                           "H3"],
                  'effets': ["touche", "touche", "touche", "touche", "coule",
                             "touche", "touche", "touche", "coule",
                             "touche", "touche", "coule",
                             "touche", "touche", "coule",
                             "touche"],
                  'pseudo': 'moi',
                  'nbreTouche': 11,
                  'nbreCoule': 5}
    elif nom == "ma_flotte":
        flotte = {'bateaux': {'porte-avions': ["A1", "B1", "C1", "D1", "E1"],
                              'croiseur': ["A3", "A4", "A5", "A6"],
                              'contre-torpilleurs': ["J8", "J9", "J10"],
                              'sous-marin': ["F1", "F2", "F3"],
                              'torpilleur': ["D2", "E2"]},
                  'tirs': [],
                  'effets': [],
                  'pseudo': 'moi',
                  'nbreTouche': 0,
                  'nbreCoule': 0}
    elif nom == "sa_flotte":
        flotte = {'bateaux': {'porte-avions': ["A1", "A2", "A3", "A4", "A5"],
                              'croiseur': ["B1", "B2", "B3", "B4"],
                              'contre-torpilleurs': ["D1", "D2", "D3"],
                              'sous-marin': ["E1", "E2", "E3"],
                              'torpilleur': ["H3", "H4"]},
                  'tirs': [],
                  'effets': [],
                  'pseudo': 'adversaire',
                  'nbreTouche': 0,
                  'nbreCoule': 0}
    else:
        flotte = {'bateaux': {'porte-avions': [],
                              'croiseur': [],
                              'contre-torpilleurs': [],
                              'sous-marin': [],
                              'torpilleur': []},
                  'tirs': [],
                  'effets': [],
                  'pseudo': '',
                  'nbreTouche': 0,
                  'nbreCoule': 0}

    return flotte

def positions_bateaux(flotte,excepte=""):
    pos_bateaux={}
    dic_bateau=flotte["bateaux"]
    for bateaux in dic_bateau.keys():
        if bateaux != excepte:#Verifie si le bateau n'est pas l'exception
            for positions in dic_bateau[bateaux]:
                pos_bateaux[positions] = bateaux#Ajoute la position et le bateau dans un dictionnaire
    return pos_bateaux

def est_bateau_coule(flotte,bateau):
    tirs=flotte["tirs"]
    pos=positions_bateaux(flotte)
    liste_pos_bateau=[]
    for position,bateaux in pos.items():
        if bateaux == bateau:
            liste_pos_bateau.append(position)
    taille_bateau=len(liste_pos_bateau)
    tir_touche=0
    for tir in tirs:
        for pos in liste_pos_bateau:
            if tir == pos:
                tir_touche+=1
    if tir_touche == taille_bateau:
        return True
    else:
        return False

def nombre_tirs_touchant_flotte(flotte):
    # Nombre de tires ayant touché une flotte
    # INIT
    touche = 0
    tirs = flotte['tirs'] # Isolation dans une var

    # Liste la position des bateaux
    for i in flotte['bateaux']:
        for pos in flotte['bateaux'][i]:
            # Liste de la position d'un bateau
            for tir in tirs:
                # Liste des tirs et comparaison avec la position
                if pos == tir:
                    touche += 1
    return touche

def nombre_bateaux_coules(flotte):
    # Renvoie le nombre de bateaux coulés
    # INIT
    coulee = 0
    tirs = flotte['tirs']

    # Liste la position des bateaux
    for i in flotte['bateaux']:
        touche = 0
        for pos in flotte['bateaux'][i]:
            # Liste de la position d'un bateau
            for tir in tirs:
                # Liste des tirs et comparaison avec la position
                if pos == tir:
                    touche += 1
                    if touche == flotte['bateaux'][i].__len__():
                        # Si le nombre de tirs touche est égale à la taille du bateau
                        # Alors le bateaux est coulé
                        #print("True", pos, tir, i) # Debug
                        coulee += 1
    return coulee

def est_flotte_coulee(flotte):
    if nombre_bateaux_coules(flotte) == 5:
        return True
    else:
        return False

def analyse_tir(flotte,ln):
    avant=nombre_tirs_touchant_flotte(flotte)
    avant2=nombre_bateaux_coules(flotte)
    flotte["tirs"].append(ln)
    apres=nombre_tirs_touchant_flotte(flotte)
    if avant==apres:
        flotte["effets"].append("eau")
        return "eau"
    else:
        flotte["nbreTouche"] += 1
        if nombre_bateaux_coules(flotte)==avant2:
            flotte["effets"].append("touche")
            return "touche"
        else:
            flotte["nbreCoule"] += 1
            if est_flotte_coulee(flotte)==True:
                flotte["effets"].append("gagne")
                return "gagne"
            elif est_flotte_coulee(flotte)==False:
                flotte["effets"].append("coule")
            return "coule"



def liste_bateaux():
    return ["contre-torpilleurs","croiseur","porte-avions","sous-marin", "torpilleur" ]

def sauvegarde_partie(ma_flotte,sa_flotte):
    f = open('sauvegarde.bin', 'wb')
    pickle.dump(ma_flotte, f)
    pickle.dump(sa_flotte, f)
    f.close()

def restauration_partie(ma_flotte,sa_flotte):
    liste1=["bateaux","tirs","effets","pseudo","nbreTouche","nbreCoule"]
    if os.path.isfile("sauvegarde.bin") == False:
        return False
    else:
        f = open("sauvegarde.bin", "rb")
        flotte1=pickle.load(f)
        flotte2=pickle.load(f)
        f.close()
        for i in liste1:
            ma_flotte[i]=flotte1[i]
            sa_flotte[i]=flotte2[i]
    return True

def tir_aleatoire(flotte):
    tirs = []
    for bateau in flotte['bateaux']:
        for tir in flotte['bateaux'][bateau]:
            tirs.append(tir)

    random = outils.case_aleatoire()
    while random in tirs:
        random = outils.case_aleatoire()
    return random

def longueur_bateau(bateau):
    taille = {
        "porte-avions": 5,
        "croiseur": 4,
        "contre-torpilleurs": 3,
        "sous-marin": 3,
        "torpilleur": 2
    }
    return taille.get(bateau)

def initialisation_flotte_vide():
    return {'bateaux': {'porte-avions': [],'croiseur': [],'contre-torpilleurs': [], 'sous-marin': [],'torpilleur': []},'tirs': [],'effets': [],'pseudo': 'anonymous','nbreTouche': 0,'nbreCoule': 0}

def est_flotte_complete(flotte):
    bateaux = flotte['bateaux']
    for bateau in bateaux:
        if (flotte['bateaux'][bateau].__len__() != longueur_bateau(bateau)):
            return False
    return True

def memorise_action_tir_sur_flotte_inconnue(flotte, case, resultat):
    flotte["effets"].append(resultat)
    flotte["tirs"].append(case)
    if resultat !="eau":
        flotte["nbreTouche"] += 1
        if resultat !="touche":
            flotte["nbreCoule"] += 1


def choix_flotte_aleatoire(flotte):
   for bateau in flotte['bateaux']:
       OK = False
       while not OK == True:
           case = outils.case_aleatoire()
           direction = ['horizontal', 'vertical'][random.randint(0,1)]
           OK = positionne_bateau_par_direction(flotte, bateau, case, direction)
   return flotte

def positionne_bateau_par_direction(flotte, bateau, case, direction):
    positions = calcul_positions_bateau(case, direction, longueur_bateau(bateau))
    if not positions.__len__():
        return False
    else:
        flotte['bateaux'][bateau] = []
        for position in positions:
            if position in positions_bateaux(flotte):
                return False
            else:
                flotte['bateaux'][bateau].append(position)
        return True

def calcul_positions_bateau(case, direction, longueur):
    case = outils.case_ln_vers_indices(case)
    pos = []
    i = 0
    if direction == "horizontal":
        case_modifier = case[1]
        case = case[0]
    else:
        case_modifier = case[0]
        case = case[1]
    while i < longueur:
        i += 1
        if direction == "horizontal":
            pos.append(outils.indices_vers_case_ln([case, case_modifier]))
        else:
            pos.append(outils.indices_vers_case_ln([case_modifier, case]))
        case_modifier += 1

    if case_modifier > 10:
        return []
    else:
        return pos

def initialisation_flotte_par_commande_du_serveur(flotte, liste):
    nbr = 0
    for par in liste:
        if par in liste_bateaux():bateau=par
        else:flotte["bateaux"][bateau].append(liste[nbr])
        nbr+=1
    return flotte
def envoi_flotte_via_serveur(flotte):
    liste=""
    for bateau in ["porte-avions","croiseur","contre-torpilleurs","sous-marin","torpilleur"]:
        liste+=bateau+"|"+"|".join(flotte["bateaux"][bateau])+"|"
    return liste[:-1]
# Programme principal pour tester vos fonctions
def main():
    print(positions_bateaux(initialisation_flotte_par_dictionnaire_fixe("flotte1")).keys())

if __name__ == '__main__':
    main()